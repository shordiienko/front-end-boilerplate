 **This boilerplate was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).**

 Current README describes a set of depencecies, tools and folder structure included to the project. It's enought setup necessary to quick start develop new project.

## Requiremeints:

You’ll need to have Node 8.10.0 or later on your local development machine, please find instructions for your [operational system](https://nodejs.org/en/download/)

## Content
 - Quick start
 - Avaliable scripts
 - Tools
 - Depencies and devDependecies
 - Project structure

## Quik start

- Install all dependencies: `npm ci`
- Run dev build: `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser

## Avaliable scripts

`npm start`

Runs the app in the development mode use 3000 port as default.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

`npm test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

`npm run build`

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

If you have necessary to customize your build process then you can run

`npm run analyze`

You can analyze the bundle run the production build then run the analyze script.

`npm run eject`

This command will remove the single build dependency from your project. Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them.

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

## Tools

`.eslintrc.json` file in the root of project

**.eslintrc.json** is used for displaying Lint Output in the Editor. 
Please, setup your Editor using [instructions](https://facebook.github.io/create-react-app/docs/setting-up-your-editor).

`pre-commit hooks`

There is only one pre-commit hook defined in **package.json**. On git add action **husky** runs **prettier** to format your code.
You can find detailed decription [here](https://facebook.github.io/create-react-app/docs/setting-up-your-editor).

Also you can intergate code formatting tool to your favorite Editor (see https://prettier.io/docs/en/editors.html)

## Dependencies and devDependecies
This boilerplate build on common React infrastructure: **react**, **react-router**, **react-redux**, **redux-thunk**, **react-loadable**.  
**Ant Design** is a used design concept. 

### Dependencies

- **antd** and **ant-design-pro** is a set of components realized Ant design concept. 
Bolirplate already use **config-overrides.js** for correct importing styles and resolve path (see https://ant.design/docs/react/use-with-create-react-app). Also **config-overrides.js** via **modifyVars** allows you to customize some basic style params including primary color, border radius, border color, etc.
(see https://ant.design/docs/react/customize-theme)  
Detailed documentation is avaliable:  
https://ant.design/docs/react/introduce  
https://pro.ant.design/docs/getting-started

- **react-loadable** is a solution for lazy module loading
Examples avaliable in App.js and [documentation](https://github.com/jamiebuilds/react-loadable)

- **react-router-dom** most popular solutiuon for routing in React app (https://reacttraining.com/react-router/web/guides/quick-start)

- **redux** is a predictable state container (https://redux.js.org/)
- **react-redux** is React bindings for Redux (https://react-redux.js.org/)
- **redux-logger** is a logger tool for Redux (https://github.com/LogRocket/redux-logger)
- **redux-thunk** provide the same interface for working both with aync and sync actions in component connected via **react-redux** (https://github.com/reduxjs/redux-thunk)

- **react-scripts** are used under the hood [Create React App](https://github.com/facebook/create-react-app)

- **axios** - is a promise based HTTP client [docs](https://github.com/axios/axios). You can use any other solution.

### devDependecies

- **customize-cra** and **react-app-rewired** are used to customise react-script for correct building **antd** and **ant-design-pro** libraries
- **husky** and **lint-staged** are used for run reformating code on precommit hooks
- **prettier** is a formatting code tool
- **source-map-explorer** is a tool for analyze and debug JavaScript code

### Files structure:
- public
    - index.js
    - manifest.json
- src
    - api
        - api1.js  
        ......  
        - apiN.js
    - assets
    - components
        - component1.js  
        ......
        - componentN.js
        - page1.js  
        ......
        - pageN.js
    - redux
        - actions
            - actionTypes.js
            - actions1.js  
            ......
            - actionsN.js
        - reducers
            - reducer1.js  
            ......
            - reducerN.js
            - initialState.js
            - index.js
        - configureStore.js
    - App.js
    - index.js
- config-overrides.js

**App.js** is a root component that:  
 - init Store connected it to React application
 - configure react-router
 - dynamicaly import (lazy loading) Home component ([docs](https://github.com/jamiebuilds/react-loadable)).

**config-overrides.js** as mention above contains configuration for correct loading antd and ant-design-pro, also it can override less variables for this libraries;

**src/api** - it would be better to store API calls in one place like this one.

**components** - directory with components. It include both components connected to redux store and not. Components connect to redux store is also colled stateful, components without redux - stateless. Stateful components includes Page suffix in naming.

**redux** - all things related to Redux stored in this directory.

**redux/configStore.js** - init store and add middlewares for develop build.

*Middlewares:*

- thunk - used for connect async calls with Redux with components
- loggerMiddleware - logging Redux state changes
- reduxImmutableStateInvariant - show warnings when Redux state is mutated

**redux/reducers/index.js** - combine reducers to one entry point

**redux/reducers/initialState.js** - initial state for Store

**redux/actions/actionTypes.js** - define all actions

### Proxy
To tell the development server to proxy any unknown requests to your API server in development, add a proxy field to your package.json. You can find detailed instructon [here](https://facebook.github.io/create-react-app/docs/proxying-api-requests-in-development).