import React, { Component } from 'react';
import { Login } from 'ant-design-pro';
import { Alert, Checkbox } from 'antd';
import PropTypes from 'prop-types';

const { UserName, Password, Submit } = Login;

class LoginForm extends Component {
  state = {
    notice: '',
    autoLogin: true
  };
  onSubmit = (err, values) => {
    if (err) {
      return;
    }

    this.props.authenticate(values.username, values.password).catch(() =>
      this.setState({
        notice: 'The combination of username and password is incorrect!'
      })
    );
  };
  changeAutoLogin = e => {
    this.setState({
      autoLogin: e.target.checked
    });
  };
  render() {
    return (
      <div className="login-warp">
        <Login onTabChange={this.onTabChange} onSubmit={this.onSubmit}>
          {this.state.notice && (
            <Alert
              style={{ marginBottom: 24 }}
              message={this.state.notice}
              type="error"
              showIcon
              closable
            />
          )}
          <UserName name="username" />
          <Password name="password" />
          <div>
            <Checkbox
              checked={this.state.autoLogin}
              onChange={this.changeAutoLogin}
            >
              Keep me logged in
            </Checkbox>
            <a style={{ float: 'right' }} href="">
              Forgot password
            </a>
          </div>
          <Submit>Login</Submit>
          <div>
            <a style={{ float: 'right' }} href="">
              Register
            </a>
          </div>
        </Login>
      </div>
    );
  }
}

LoginForm.propTypes = {
  authenticate: PropTypes.func.isRequired
};

export default LoginForm;
