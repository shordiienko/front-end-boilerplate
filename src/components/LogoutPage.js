import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../redux/actions/authActions';
import Logout from './Logout';

function LogoutPage({ isLoggedIn, logout }) {
  return <Logout isLoggedIn={isLoggedIn} logout={logout} />;
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.isLoggedIn
  };
};

const mapDispatchToProps = {
  logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutPage);
