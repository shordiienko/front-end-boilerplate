import React, { useEffect } from 'react';
import { Redirect } from 'react-router-dom';

function Logout({ isLoggedIn, logout }) {
  useEffect(() => {
    if (isLoggedIn) {
      logout();
    }
  });
  return isLoggedIn ? null : <Redirect to="/login" />;
}

export default Logout;
