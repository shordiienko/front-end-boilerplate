import React from 'react';
import LoginForm from './Login';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from '../redux/actions/authActions';
import PropTypes from 'prop-types';

function LoginPage({ isLoggedIn, login, ...props }) {
  let { from } = props.location.state || { from: { pathname: '/' } };

  function authenticate(email, password) {
    let loginPromise = login(email, password);
    return loginPromise;
  }

  if (isLoggedIn) {
    return <Redirect to={from} />;
  }
  return <LoginForm authenticate={authenticate} />;
}

LoginPage.propTypes = {
  isLoggedIn: PropTypes.bool,
  login: PropTypes.func
};

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.isLoggedIn
  };
};

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
