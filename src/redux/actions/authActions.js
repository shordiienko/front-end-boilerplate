import * as types from './actionTypes';
import * as authApi from '../../api/authApi';

export function loginSuccess(data) {
  return { type: types.LOGIN_SUCCESS, data };
}

export function logoutFailed(error) {
  return { type: types.LOGOUT_FAILDED, error };
}

export function loginFailed(error) {
  return { type: types.LOGIN_FAILED, error };
}

export function logoutSuccess() {
  return { type: types.LOGOUT_SUCCESS };
}

export function logout() {
  return function(dispatch) {
    dispatch(logoutSuccess());
    return authApi.logout().catch(error => {
      dispatch(logoutFailed(error));
      throw error;
    });
  };
}

export function login(email, password) {
  return function(dispatch) {
    return authApi
      .login(email, password)
      .then(data => {
        dispatch(loginSuccess(data));
      })
      .catch(error => {
        dispatch(loginFailed(error));
        throw error;
      });
  };
}
