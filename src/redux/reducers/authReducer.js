import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function courseReducer(state = initialState.auth, action) {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        email: action.data.email,
        accessToken: action.data.accessToken,
        error: null
      };
    case types.LOGIN_FAILED:
      return {
        ...state,
        isLoggedIn: false,
        accessToken: null,
        error: action.error
      };
    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
        accessToken: null,
        error: null
      };
    case types.LOGOUT_FAILDED:
      return {
        error: action.error
      };
    default:
      return state;
  }
}
