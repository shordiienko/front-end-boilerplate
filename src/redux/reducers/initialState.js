export default {
  auth: {
    isLoggedIn: false,
    email: null,
    accessToken: null,
    error: null
  }
};
