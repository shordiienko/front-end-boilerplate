import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import LoginPage from './components/LoginPage';
import LogoutPage from './components/LogoutPage';
import Loadable from 'react-loadable';
import PrivateRoute from './components/PrivateRoute';
import NotFound from './components/NotFound';
import './App.css';
import configureStore from './redux/configureStore';
import { Provider as ReduxProvider } from 'react-redux';

// Lazy loading example
// module Home will be loaded only if route /home reached
const Home = Loadable({
  loader: () => import('./components/Home'),
  loading: () => '<div>Content Loading...</div>'
});

// create Redux Store
const store = configureStore();

function App() {
  return (
    // connect store via ReduxProvider
    <ReduxProvider store={store}>
      <Router>
        <Switch>
          <Redirect exact from="/" to="/home" />
          <Route path="/login" component={LoginPage} />
          <Route path="/logout" component={LogoutPage} />
          <PrivateRoute path="/home" component={Home} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </ReduxProvider>
  );
}

export default App;
